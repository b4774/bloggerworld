## Name
BLOGGER WORLD

## Description
Developed a feature-rich blog application using the MERN (MongoDB, Express.js, React, Node.js) stack. This dynamic platform enables users to create, edit, and delete blog posts. Registered users can explore a variety of functionalities including:

Viewing other blogs and accessing the author's posts.
Uploading and managing profile pictures.
Browsing favorite categories with ease.
The app employs RESTful APIs for smooth communication between the frontend and backend, ensuring a seamless user experience.

## Key Features
User Authentication and Authorization
Create, Edit, and Delete Blog Posts
View Other Blogs and Author's Posts
Profile Picture Upload and Management
Browse Favorite Categories
Responsive UI Design
RESTful API Implementation
MongoDB Database Integration

## Technologies
React
Node.js
Express.js
MongoDB
HTML/CSS
JavaScript

## Outcome
Successfully developed a MERN stack blog application with an array of features including the ability to view and interact with other blogs and their authors, profile picture management, and category-based browsing. This project showcases a high level of proficiency in full-stack development and modern web technologies.
